
import Post from "../service/Post";
import params from "../sustem/params";

export const upPaggerAction = (Pagger) => dispatch => 
{   
    let post = new Post();
    post.body = JSON.stringify({Pagger:Pagger});

    fetch(params.url+'transactions/list', post
    ).then(function(response)
    { 
        return response.json();
    }
    ).then(d => {       
        d.paginator.limit = Pagger.limit;
        dispatch({type:'UP_PAGGER', data:d.paginator});
    });
}