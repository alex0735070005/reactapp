import Post from "../service/Post";
import params from "../sustem/params";

export const getDataAction = () => dispatch => 
{   
    let post = new Post();

    fetch(params.url + 'deposit/getdata', post
    ).then(responce => {
            return responce.json();
        }
    ).then(d => {
        dispatch({type:'GET_DEPOSIT_DATA', data:d});
    });
}

export const getRubCourseAction = () => dispatch => 
{   
    let post = new Post();

    fetch(params.url + 'api/get-rub-course', post
    ).then(responce => {
            return responce.json();
        }
    ).then(d => {
        dispatch({type:'GET_RUB_COURSE', data:d});
    });
}

export const getCriptCourseAction = (code) => dispatch => 
{   
    let post = new Post();

    post.body = JSON.stringify(code);


    fetch(params.url + 'api/get-cript-course', post
    ).then(responce => {
            return responce.json();
        }
    ).then(d => {
        dispatch({type:'GET_CRIPT_COURSE', data:d});
    });
}

export const generateWalletAction = (propsWallet, changeLoad) => dispatch => 
{   
    changeLoad();

    let post = new Post();

    post.body = JSON.stringify({id:propsWallet.id, currency_id:propsWallet.currency_id, type:propsWallet.code});

    fetch(params.url + 'deposit/generate', post
    ).then(function (response)
    {
        return response.json();
    }
    ).then(resp => {
        dispatch({type:'CHANGE_WALLET', data:{label: resp.label, address:resp.address, wkey:propsWallet.wkey}})
        changeLoad();
    });
}

export const sendSwapAction = (sendData) => dispatch => 
{   
    let post = new Post();

    post.body = JSON.stringify(sendData);

    fetch(params.url + 'pay/pay', post
    ).then(function (response)
    {
        return response.json();
    }
    ).then(resp => 
    {
        var message = 'Для просмотра статуса платежа прейдите на вкладку история';
        var title = 'Данные отправленны';

        if(resp.error === false){            
            dispatch({type:'CHANGE_BALANCES', data:resp.balances});
        }else{
            message = 'Не хватает денег на балансе';
            title = 'Не возможно выполнить операцию !'
        }

        dispatch({type:'SET_DATA_INFO', data:{title:title, message:message}});
        dispatch({type:'SHOW_INFO'});
        dispatch({type:'CHANGE_NAV_NAME_CURRENCY', data:{navname:'swap', code:sendData.curentCurrencyCode}});

        setTimeout(()=>{
            dispatch({type:'HIDE_INFO'});            
        },4000);
    });
}

export const qiwiPayAction = (dataPay) => dispatch => 
{   
    let post = new Post();

    post.body = JSON.stringify(dataPay);

    fetch(params.url + 'deposit/qiwipay', post
    ).then(responce => {
            return responce.json();
        }
    ).then(d => {
        alert('Данные пополнения карточкой на qiwi на обработке')
    });
}

export const webMoneyPayAction = (dataPay) => dispatch => 
{   
    let post = new Post();

    post.body = JSON.stringify(dataPay);

    fetch(params.url + 'deposit/webmoneypay', post
    ).then(responce => {
            return responce.json();
        }
    ).then(d => {
        alert('Данные пополнения карточкой на WebMoney на обработке')
    });
}
