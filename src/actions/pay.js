import Post from "../service/Post";
import params from "../sustem/params";

export const getDataAction = () => dispatch => 
{   
    let post = new Post();

    fetch(params.url + 'pay/getdata', post
    ).then(responce => {
            return responce.json();
        }
    ).then(d => {
        dispatch({type:'GET_PAY_DATA', data:d});
    });
}

export const getBalanceAction = (data) => dispatch => 
{   
    let post = new Post();

    post.setBody(data);

    fetch(params.url + 'pay/getbalance', post
    ).then(responce => {
            return responce.json();
        }
    ).then(d => {
        //dispatch({type:'GET_PAY_DATA', data:d});
    });

    //dispatch({type:'SET_BALANCE_WALLET', data:{}});
}

export const sendPayAction = (pay) => dispatch => 
{   
    let post = new Post();
    post.setBody(pay);

    fetch(params.url + 'pay/pay', post
    ).then(function (response)
    {
        return response.json();
    }
    ).then(data => {
        
        if(!data.error && !data['get_pay_code'])
        {
            dispatch({type:'SET_DATA_INFO', data:{title:'Данные отправленны', message:'Для просмотра статуса платежа прейдите на вкладку история'}});
            dispatch({type:'SHOW_INFO'});
            dispatch({type:'CHANGE_BALANCES', data:data.balances});

            setTimeout(()=>{
                dispatch({type:'HIDE_INFO'});
            },4000);
        }
        else if(data['get_pay_code']){
            //this.setState({showPopup:'show'});
        }
        else{
            //this.showMessage(this.state.titles[data.error], 'alert alert-danger');
        }
    });
}