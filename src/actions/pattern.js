import Pattern from '../service/Pattern';
import Post from '../service/Post';
import params from "../sustem/params";

export const addPatternAction = (data, recipient) => dispatch => 
{   
    let post = new Post();

    let pattern = new Pattern();
    
    pattern.setName(data.name);
    pattern.setComment('');
    pattern.setType(1);
    pattern.setValue(recipient);

    post.body = JSON.stringify(pattern);

    fetch(params.url+'pay/add-pay-pattern', post
    ).then(function (response)
    {
        return response.json();
    }
    ).then(d => {
        return dispatch({type:'ADD_PATTERN', data:d})
    }).then(d => {
           
            if(d.data.errors === false)
            {
                dispatch({type:'TRIGGER_SUCCESS_ADD_PATTERN'});
        
                setTimeout(()=>{  
                    dispatch({type:'TRIGGER_SUCCESS_ADD_PATTERN'});
                    dispatch({type:'TRIGGER_ADD_POPUP_PATTERN', data:{key:'', show:false}});
                    dispatch({type:'CHANGE_PATTERN_NAME', data:{name:''}});
                }, 4000);
            }
            else
            {
                dispatch({type:'TRIGGER_ERROR_ADD_PATTERN'});
            }
        }
    );
}

export const editPatternAction = (data, form, pkey) => dispatch => 
{   
    let post = new Post();

    let newPattern = 
        {
            'id':data.id,
            'name':form.name.value,
            'comment':data.comment,
            'type':data.type
        };

    post.body = JSON.stringify(newPattern);

    fetch(params.url+'pay/update-pay-pattern', post
    ).then(function (response)
    {
        return response.json();
    }
    ).then(d => {

            if(d.errors === false)
            {
                dispatch({type:'EDIT_PATTERN', data:{pkey:pkey, pattern:newPattern}})
                dispatch({type:'TRIGGER_SUCCESS_EDIT_PATTERN'});
        
                setTimeout(()=>{  
                    dispatch({type:'TRIGGER_SUCCESS_EDIT_PATTERN'});
                    dispatch({type:'TRIGGER_EDIT_POPUP_PATTERN', data:{key:'', show:false}});
                }, 4000);
            }
            else
            {
                dispatch({type:'TRIGGER_ERROR_EDIT_PATTERN'});
            }
        }
        
    );
}

export const removePatternAction = (data, rkey, pkey) => dispatch => 
{   
    let post = new Post();

    post.body = JSON.stringify({
        'pattern_id':data.id
    });

    fetch(params.url+'pay/remove-pay-pattern', post
    ).then(function (response)
    {
        return response.json();
    }
    ).then(d => {

            setTimeout(()=>{  
                dispatch({type:'REMOVE_PATTERN', data:{pkey:pkey, rkey:rkey}});
                dispatch({type:'TRIGGER_REMOVE_POPUP_PATTERN', data:{key:'', show:false}});
            }, 1000);
        }
    );
}
