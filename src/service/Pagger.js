import params from "../sustem/params";
class Pagger {
    constructor()
    {
      this.limit = 10;
      this.sorted = [{desc:true, id:'date_update'}];
      this.filtered = [];
      this.data = [];
      this.page = 1;
      this.pages = null;
      this.loading = false;
      this.columns = [
          {
            Header: "Дата",
            id: 'date_update',
            accessor: d => this.showDate(d.date_update)
          },
          {
            Header: "Тип операции",
            id: 'type_operation',
            accessor: d => this.getType(d),
          },              
          {
            Header: "Способ оплаты",
            accessor: "method_short_name"
          },
          {
            Header: "Сумма",
            id: 'amount',
            accessor: d => this.getAmount(d)
          },
          {
            Header: "Статус",
            accessor: "status_name"
          }
        ];
    }

    showDate(data)
    {
        var res = new Date(Date.parse(data.date));
        
        var options = {           
            day:    'numeric',
            month:  'numeric',
            year:   'numeric',
            hour:   'numeric',
            minute: 'numeric',
            second: 'numeric'
        };
        
        return res.toLocaleString("ru", options); 
    }

    getType(data)
    {
        if(data.type_operation){
          return 'Пополнение ' + data.code;
        }      
        return 'Оплата ' + data.code;
    }

    getAmount(data)
    {
        return (+data.amount).toFixed(params.numLength) + ' ' + data.code;
    }
    
}

export default Pagger;