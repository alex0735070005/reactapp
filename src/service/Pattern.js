/**
 * Class Pay
 *
 * Author Alex Axenov
 */
class Pattern 
{    
    constructor()
    {
        this.id      = '';
        this.user_id = '';
        this.name    = '';
        this.comment = '';
        this.value   = '';
        this.type    = '';
    }
    
    setId(id){
        this.id = id;
    }
    
    setUser(user_id){
        this.user_id = user_id;
    }
    
    setName(name){
        this.name = name;
    }
    
    setValue(value){
        this.value = value;
    }
    
    setType(type){
        this.type = type;
    }
    
    setComment(comment){
        this.comment = comment;
    }
}

export default Pattern;