/**
 * Class Pay
 *
 * Author Alex Axenov
 */
class Pay 
{    
    constructor ()
    {
        this.id              = '';
        this.name            = '';
        this.balances        = [];
        this.errors          = {};
        this.recipients      = [];
        this.currencies      = [];
        this.patterns        = [];
        this.pay_password    = '';
        this.payment_methods = [];
        this.confirm_code    = '';
        this.type_operation  = 0;
    }
}

export default Pay;