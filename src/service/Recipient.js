
let params  = require('../sustem/params');

/**
 * Class Recipient
 *
 * Author Alex Axenov
 */
class Recipient 
{    
    constructor ()
    {
        this.recipient_name    = '';
        this.email             = '';
        this.card_date         = '';
        this.pay_method        = 0;
        this.currency          = params.currency;
        this.pay_method_code   = '';
        this.pay_method_slug   = 'sustem';
        this.effect            = '';
        this.number            = '';
        this.amount            = '';
        this.balance            = '';
        this.errors = {
            email:false,
            number:false,
            amount:false,
            card_date:false
        };
    }
}

export default Recipient;