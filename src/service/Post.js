
let params  = require('../sustem/params');

class Post 
{
    constructor(){
        this.method      = 'POST';
        this.credentials = params.same;
        this.body = '';
    }

    setBody(d){
        this.body = JSON.stringify(d);
    }
}

export default Post;