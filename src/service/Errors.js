/**
 * Class Errors
 *
 * Author Alex Axenov
 */
class Errors 
{    
    constructor ()
    {
        this.email = false;
        this.type_pay = false;
        this.number = false;
        this.amount = false;
        this.cart_date = false;
    }
    
    
}

export default Errors;