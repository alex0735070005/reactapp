class Validator 
{
    validSum = (str) => !str.match(/^[0-9]{0,}[.]{0,}[0-9]{0,}$/) ? false : true;
}

export default new Validator();