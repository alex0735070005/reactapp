import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter} from 'react-router-dom';

import App from './containers/App';

// Импорт провайдера из react-redux
import {Provider} from 'react-redux';

// Метод для добовления dev tools
import { composeWithDevTools } from 'redux-devtools-extension';

// Импорт хранилища из redux
import { createStore, applyMiddleware } from 'redux';

// Подключаем главный reduser
import reducer from './reducers';

// Промежуточный слой для написания асинхронных запросов для redux
import thunk from 'redux-thunk';

// Создаем объект хранилища, передаем туда наш reduser и подключаем инструменты разработки redux
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));


ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <App />
        </HashRouter>
    </Provider>, 
    document.getElementById('operations_root')
);