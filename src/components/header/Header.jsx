import React, {Component} from 'react';
import NavHeader from './NavHeader';

class Header extends Component {

    render() {
      return (
            <div>
                <NavHeader />
            </div>
          );
    }
}

export default Header;