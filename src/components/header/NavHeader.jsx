import React, {Component} from 'react';
import {connect} from 'react-redux';

const active = "btn btn-success active";
const defnav = "btn btn-success";

class NavHeader extends Component {

    showNavList = () => {
        return this.props.navList.map((item, k)=><a key={k} data-nkey={k} className={item.active ? active : defnav} href={'#'+item.url}>{item.name}</a>);
    }

    render() {
      return (
            <div>
                <div className="row block-tab">
                    {this.showNavList()}
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        navList:state.header.navList
    }),
    dispatch => ({
        
    })
)(NavHeader);