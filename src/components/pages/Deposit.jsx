import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getDataAction, getRubCourseAction} from "../../actions/deposit";
import Currency from "../modules/Currency";
import PopupInfo from '../modules/PopupInfo';
class Deposit extends Component {

   
    componentDidMount()
    {
        this.props.getData();
        this.props.changeActive('/deposit');
        this.props.getRubCourse();
    }

    getChangeCurrency = (code) => 
    {        
        switch(code){
            case 'RUB': return this.getCurrencyByCode('USD'); break;
            case 'USD': return this.getCurrencyByCode('RUB'); break;
        }
    }

    getCurrencyByCode = (code) => 
    {
        let currency = {};

        this.props.currencies.forEach(c => {
            if(code === c.code) currency = c;
        });

        return currency;
    }

    getCurrencies = () => {
      return this.props.currencies.map(
          (currency, k) => <Currency key={k} ckey={k} data={currency} dataChange = {this.getChangeCurrency(currency.code)} />
      );
    }

    render() {
      return (
            <div className="profileblock">
                {this.getCurrencies()}
                <PopupInfo />
            </div>
          );
    }
  }
  
  // Принимем стейт из хранилища  redux 
  // и устанавливаем в параметры текущего компонента
  // из хранилища нужные данные для отрисовки
  export default connect(
    state => ({
          currencies:state.deposit.currencies
    }),
    dispatch => ({
        getData: () => {
            dispatch(getDataAction());
        },
        changeActive: (active) => {
            dispatch({type:'CHANGE_ACTIVE', data:{nkey:active}});
        },
        getRubCourse: () => {
            dispatch(getRubCourseAction());
        }
    })
  )(Deposit);