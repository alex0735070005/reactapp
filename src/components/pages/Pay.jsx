import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getDataAction, sendPayAction} from "../../actions/pay";
import Recipients from '../modules/pay/Recipients';
import PopupAddPattern from '../modules/pay/PopupAddPattern';
import PopupEditPattern from '../modules/pay/PopupEditPattern';
import PopupRemovePattern from '../modules/pay/PopupRemovePattern';
import PopupInfo from '../modules/PopupInfo';
import Balances from '../modules/Balances';
import SumPayments from '../modules/SumPayments';



class Pay extends Component {

    constructor(props)
    {
        super(props);
       
        this.state = {
            regular:false, 
        };
     
    }
   
    componentDidMount()
    {
        this.props.getData();
        this.props.changeActive('/pay');
    }

    valid = () =>
    {
        var vfields = true;
        
        for (var j in this.props.recipients)
        {
            var recipient = this.props.recipients[j];

            this.props.nullErrorRecipient(j);

            var code = recipient.pay_method_slug;
            
            if(code === 'sustem' && !recipient.email.match(/^[a-zA-Z0-9_\-.]{2,}@[a-zA-Z0-9_\-.]{1,}.[a-zA-Z]{2,6}$/))
            {
                vfields = false;
                this.props.changeErrorRecipient({rkey:j, name:'email', value:true});
            }
            
            /*if(code !== 'sustem' && !recipient.number.toString().replace(/[^\d]/g, '').match(/^[0-9]{2,}$/)){
                 vfields = false;
                 this.props.changeErrorRecipient({rkey:j, name:'number', value:true});
            }*/
            
            if(!recipient.amount.match(/^[0-9\.]{1,}$/)){
                 vfields = false;
                 this.props.changeErrorRecipient({rkey:j, name:'amount', value:true});
            }

            if(code === 'card' && recipient.pay_method_code==="AlfaBankCard" && !recipient.card_date.match(/^[0-9]{2}\/[0-9]{2}$/)){
                 vfields = false;
                 this.props.changeErrorRecipient({rkey:j, name:'card_date', value:true});
            }
        }

        return vfields;
    }

    sendPayment = () =>
    {
        if(this.valid())
        {
            var pay  = this.props.pay;
            this.props.sendPay(pay);
        }
    }

    render() {
      return (
            <div className="profileblock pay_block">
                  <div className="payment">
                        <div className="row">
                            <h2 className="col-sm-8 pay-title" >Оплатить</h2>
                            <div className="col-sm-4">
                                <Balances />                            
                            </div>
                        </div>
                        <Recipients />

                        <SumPayments balances={this.props.balances}/>

                        <div className="form-group hidden">
                            <label>
                                <input  type="checkbox" name="reg_pay" checked={this.state.regular} onChange={this.changeReg} />
                                {this.props.titles.regular_pay}
                            </label>
                        </div>
                        <div className="form-bottom">
                            <div className={this.state.show} role="alert">{this.state.message}</div>
                            <button onClick={this.sendPayment}  className="btn btn-success" type="button">{this.props.titles.send}</button>
                        </div>

                        <PopupAddPattern  />
                        <PopupEditPattern  />
                        <PopupRemovePattern  />
                        <PopupInfo  />

                  </div>
            </div>
          );
    }
  }
  
  export default connect(
      state => ({   
          titles:state.language,
          pay:state.pay,
          pattern:state.pattern,
          recipients:state.pay.recipients,
          balances:state.pay.balances
      }),
      dispatch => ({
        getData: () => {
            dispatch(getDataAction());
        },
        sendPay: (pay) => {
            dispatch(sendPayAction(pay));
        },
        changeErrorRecipient: (d) => {
            dispatch({type:'CHANGE_ERROR_RECIPIENT', data:d});
        },
        nullErrorRecipient: (k) => {
            dispatch({type:'NULL_ERROR_RECIPIENT', data:{rkey:k}});
        },
        showPaySuccess: (k) => {
            dispatch({type:'SHOW_INFO'});
        },
        setDataInfo: (d) => {
            dispatch({type:'SET_DATA_INFO', data:d});
        },
        changeActive: (active) => {
                dispatch({type:'CHANGE_ACTIVE', data:{nkey:active}});
        }        
      })
  )(Pay);