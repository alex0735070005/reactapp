import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactTable from "react-table";
import "react-table/react-table.css";
import {upPaggerAction} from "../../actions/transactions";

class Transactions extends Component {

    componentDidMount()
    {
        this.props.changeActive('/transactions');
    }

    changePagger = (state) =>
    {       
        this.props.upPagger(
        {
            limit:          state.pageSize,
            pages:          state.pages,
            page:           state.page + 1,
            sorted:         state.sorted,
            filtered:       state.filtered
                
        });
    }
    
    render() {
      
        return (
                <div className="profileblock">
                    <h3>{this.props.name}</h3>
                    <ReactTable
                        columns={this.props.pagger.columns}
                        manual
                        data={this.props.pagger.data}
                        pages={this.props.pagger.pages}
                        filterable
                        onFetchData={this.changePagger}
                        defaultPageSize={this.props.pagger.limit}
                        className="-striped -highlight"
                        SubComponent={row => {                    
                            return(
                                <div>My Component</div>
                            )
                        }}
                        />
                </div>
            );
        
    }
}

export default connect(
    state => ({
        setting:state.setting,
        pagger:state.transactions.pagger,
        name:state.transactions.name
    }),
    dispatch => ({
        upPagger:(data) => {            
            dispatch(upPaggerAction(data));
        },
        changeActive: (active) => {
            dispatch({type:'CHANGE_ACTIVE', data:{nkey:active}});
        } 
    })
)(Transactions);