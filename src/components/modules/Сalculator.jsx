import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getRubCourseAction, sendSwapAction, getCriptCourseAction} from '../../actions/deposit';
import Validator from '../../service/Validator';


class Calculator extends Component {

    constructor(props)
    {
        super(props);

        this.state = {
            cript_value:'',
            cript_code:'',
            calc_value:'',
            showAmountErr:false,
            showCodeErr:false
        }
    }

    changeLeftValue = (el) => 
    {       
        let result = '';
        
        this.setState({showAmountErr:false, showCodeErr:false});
        
        if(!this.state.cript_code){
            this.setState({showCodeErr:true});  return;
        }


        if(this.props.currency.code === 'RUB' && this.state.cript_code && this.props.cript_course){
            result = el.target.value * this.props.cript_course * this.props.rub_course;
        }

        if(this.props.currency.code === 'USD' && this.state.cript_code && this.props.cript_course){
            result = el.target.value * this.props.cript_course;
        }

        
        Validator.validSum(el.target.value) ? this.setState({cript_value:el.target.value, calc_value:result ? result:''}) : this.setState({showAmountErr:true});        
        
    }

    changeRightValue = (el) => 
    {       
        let result = '';
        
        this.setState({showAmountErr:false, showCodeErr:false});
        
        if(!this.state.cript_code){
            this.setState({showCodeErr:true});  return;
        }


        if(this.props.currency.code === 'RUB' && this.state.cript_code && this.props.cript_course){
            result = el.target.value / this.props.rub_course / this.props.cript_course;
        }

        if(this.props.currency.code === 'USD' && this.state.cript_code && this.props.cript_course){
            result = el.target.value / this.props.cript_course;
        }
        
        Validator.validSum(el.target.value) ? this.setState({cript_value:result ? result:'', calc_value:el.target.value}) : this.setState({showAmountErr:true});        
        
    }


    changeCript = (el) => {
        this.setState({showAmountErr:false, showCodeErr:false, cript_value:'', calc_value:''});
        this.setState({cript_code:el.target.value})
        this.props.getCriptCourse({code:el.target.value});
        this.props.getRubCourse();
    }

    getWallets = () => {
        return this.props.wallets.map(
            (wallet, k) => {
                if(wallet.currency_id === this.props.currency.id){
                    return <option key={k} value={wallet.code}>{wallet.title}</option>
                }    
            }
        );
    }

    getCalcCourse = () => {

        if(this.props.currency.code === 'RUB' && this.state.cript_code && this.props.cript_course){
            return "1 " + this.state.cript_code + " = " + (1 * this.props.cript_course * this.props.rub_course).toFixed(2) + ' RUB';
        }

        if(this.props.currency.code === 'USD' && this.state.cript_code && this.props.cript_course){
            return "1 " + this.state.cript_code + " = " + (+this.props.cript_course).toFixed(6) + ' USD';
        }

        return '';
    }

    render() {
        return (          
            <div className="swap-block">  
                <div className="col-sm-4">
                    <div className="block-list-calc">
                    <input  className="form-control" onInput={this.changeLeftValue} data-code={this.props.currency.code} type="text" value={this.state.cript_value} />
                        <select onChange={this.changeCript} className="form-control">
                            <option value="">Криптовалюта</option>
                            {this.getWallets()}
                        </select>
                        
                    </div>
                    {this.state.showAmountErr ? <span className="text-danger" >Не валидное значение пример: 00.00</span> : ''} 
                    {this.state.showCodeErr ? <span className="text-danger" >Выберите криптовалюту</span> : ''}                                                       
                </div>
                <div className="col-sm-4 exchange-block">
                    <p>
                        <i className="fa fa-exchange" aria-hidden="true"></i>
                    </p>
                    <p>Курс:<br/>
                       {this.getCalcCourse()}
                    </p>
                </div>
                <div className="col-sm-4">
                    <label><i className={this.props.currency.code === 'USD' ? "fa fa-usd" :"fa fa-rub" } aria-hidden="true"></i> {this.props.currency.title + ' ' + this.props.currency.code}</label><br/>
                    <input onInput={this.changeRightValue} type="text" value={this.state.calc_value} />
                </div>
               
            </div>
        );
    }
}

export default connect(
    state => ({
        titles:state.language,
        rub_course:state.deposit.rub_course,
        cript_course:state.deposit.cript_course,
        wallets:state.deposit.wallets 
    }),
    dispatch => ({
        getRubCourse: () => {
            dispatch(getRubCourseAction());
        },
        getCriptCourse: (d) => {
            dispatch(getCriptCourseAction(d));
        }
    })
  )(Calculator);