import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getRubCourseAction, sendSwapAction} from '../../actions/deposit';
import Validator from '../../service/Validator';


class Swap extends Component {

    constructor(props){
        super(props);
        this.state = {
            showRubErr: false
        }
    }

    componentDidMount()
    {
        this.props.getRubCourse();
    }

    changeValue = (el) => 
    {
        this.setState({showRubErr:false});
        Validator.validSum(el.target.value) ? this.props.changeCurrencyValue({code:el.target.dataset.code, value:el.target.value}) : this.setState({showRubErr:true});
    }

    send = () => {

        let sendData = {
            typeTransaction:'swap',
            typeSwap:this.props.currency.code + this.props.currencyChange.code,
            currentCurrencyCode:this.props.currency.code,
            currencies:{}
        }
        
        sendData['currencies'][this.props.currency.code] = this.props.currency;
        sendData['currencies'][this.props.currencyChange.code] = this.props.currencyChange;
        this.props.sendSwap(sendData);
    }

    render() {
        return (          
            <div className="swap-block">  
                <div className="col-sm-4">
                    <label><i className={this.props.currency.code === 'USD' ? "fa fa-usd" : "fa fa-rub" } aria-hidden="true"></i> {this.props.currency.title + ' ' + this.props.currency.code}</label><br/>
                    <input className="form-control" onInput={this.changeValue} data-code={this.props.currency.code} type="text" value={this.props.currency.value} />
                    {this.state.showRubErr ? <span className="text-danger" >Не валидное значение пример: 00.00</span> : ''}
                </div>
                <div className="col-sm-4 exchange-block">
                    <p>
                        <i className="fa fa-exchange" aria-hidden="true"></i>
                    </p>
                    <p>Курс:<br/>
                        {this.props.currency.code === 'RUB' ?   "1 RUB = " + (1/this.props.rub_course).toFixed(4) + ' USD' : "1 USD = " + this.props.rub_course + ' RUB'}
                    </p>
                </div>
                <div className="col-sm-4">
                    <label><i className={this.props.currencyChange.code === 'USD' ? "fa fa-usd" :"fa fa-rub" } aria-hidden="true"></i> {this.props.currencyChange.title + ' ' + this.props.currencyChange.code}</label><br/>
                    <input className="form-control" type="text" value={this.props.currencyChange.value} readOnly />
                </div>
                <div className="col-sm-12">
                    <button className="center-block" onClick={this.send}>{this.props.titles.send}</button>
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        titles:state.language,
        rub_course:state.deposit.rub_course
    }),
    dispatch => ({
        getRubCourse: () => {
            dispatch(getRubCourseAction());
        },
        changeCurrencyValue: (d) => {
            dispatch({type:'CHANGE_CURRENCY_VALUE', data:d});
        },
        sendSwap: (d) => {
            dispatch(sendSwapAction(d));
        },
    })
  )(Swap);