import React, {Component} from 'react';
import {connect} from 'react-redux';
import Calculator from './Сalculator';
import PaymentList from './PaymentList';
import WalletList from './WalletList';
import Swap from './Swap';


class Currency extends Component {

    constructor(props)
    {
        super(props);
    }

    show = (e) => {
       
        this.props.changeNavNameCurrency({navname:e.target.dataset.nav, code:this.props.data.code});
       
    }

    getBalanceValue = () => {

        let value = 0;

        this.props.balances.forEach(balance => {
            if(balance.currency_code === this.props.data.code) value = balance.value;
        });

        return value;
    }

    render() {
        return (
            <div className="currency-block">
                <div className = "top-currency row">
                    <div className="currency-name col-sm-6">
                        <i className={'fa ' + this.props.data.sumbol}>
                        </i> 
                        <span className="currency-title">
                            {this.props.data.title}
                        </span>
                        <span className="currency-code">
                            ({this.props.data.code})
                        </span>
                    </div>
                    <div className="currency-balance col-sm-6 text-right">
                        {this.props.data.code + ' ' + this.getBalanceValue()}
                    </div>
                </div>

                <div className = "currency-nav col-sm-12">
                    <button type="button" data-nav="wallet" onClick={this.show} className={this.props.data.navname    === "wallet"   ? "active btn btn-default wallet":"btn btn-default wallet"}>{this.props.titles.pull_cript}</button>
                    <button type="button" data-nav="pay"    onClick={this.show} className={this.props.data.navname    === "pay"      ? "active btn btn-default pay":"btn btn-default pay"}>{this.props.titles.pull_fiat}</button>
                    <button type="button" data-nav="calc"   onClick={this.show} className={this.props.data.navname    === "calc"     ? "active btn btn-default calc":"btn btn-default calc"}>{this.props.titles.calculator}</button>
                    <button type="button" data-nav="swap"   onClick={this.show} className={this.props.data.navname    === "swap"     ? "active btn btn-default swap":"btn btn-default swap"}>{this.props.titles.trade_on} {this.props.data.code === 'USD' ? "RUB" :"USD" }</button>
                </div>

                { (this.props.data.navname === 'wallet') ? <WalletList data = {this.props.data} /> : '' }
               
                { (this.props.data.navname === 'pay') ? <PaymentList code = {this.props.data.code} />:'' }

                { (this.props.data.navname === 'calc') ? <Calculator currency={this.props.data} />:'' }

                { (this.props.data.navname === 'swap') ? <Swap currency={this.props.data} currencyChange={this.props.dataChange} />:'' }
            </div>
        );
    }
}

export default connect(
    state => ({
        wallets:state.deposit.wallets,
        titles:state.language,
        balances:state.deposit.balances        
    }),
    dispatch => ({
        changeActive: (active) => {
            dispatch({type:'CHANGE_ACTIVE', data:active});
        },
        changeNavNameCurrency: (d) => {
            dispatch({type:'CHANGE_NAV_NAME_CURRENCY', data:d});
        }
    })
  )(Currency);