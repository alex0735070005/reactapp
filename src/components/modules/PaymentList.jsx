import React, {Component} from 'react';
import {connect} from 'react-redux';
import QiwiPay from './paymethods/QiwiPay';
import QiwiWallet from './paymethods/QiwiWallet';
import WebMoneyPay from './paymethods/WebMoneyPay';


class PaymentList extends Component {


    render() {
        return (
            <div className="payments-block">
                {this.props.code === 'RUB'? <QiwiPay />: ''}
                {this.props.code === 'RUB'? <QiwiWallet />: ''}
                <WebMoneyPay code = {this.props.code} /> 
            </div>
        );
    }
}

export default connect(
    state => ({
       
    }),
    dispatch => ({
       
    })
  )(PaymentList);