import React, { Component } from 'react';
import {connect} from 'react-redux';

class PopupInfo extends Component {

    render()
    {   
        let show = this.props.info.show ? 'show' : 'hidden';

        if (this.props.info.show)
        {
            return (
                <div className={'popup_background ' + show}>
                    <div className = "popup_wrap">
                        <div className = "popup_body">
                            <h4>{this.props.info.title}</h4>
                            <p>{this.props.info.message}</p>
                            <button type="button" className="btn btn-primary pull-right" onClick={this.props.closePopup}>{this.props.titles.close}</button>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            return null;
        }
    }
};

 export default connect(
    state => ({
        titles:state.language,
        info:state.info
    }),
    dispatch => ({
        closePopup: () => {
            dispatch({type:'HIDE_INFO'});
        }
    })
  )(PopupInfo);