import React, { Component } from 'react';
import {CopyToClipboard} from 'react-copy-to-clipboard';

class Ticker extends Component
{

    constructor(props)
    {
        super(props);
        
        this.state = {
            tickers:[],
            titles:{
                    tBTCUSD:{'name':'Биткойн', 'code':'BTC'}, 
                    tLTCUSD:{'name':'Лайткойн', 'code':'LTC'}, 
                    tETHUSD:{'name':'Эфир', 'code':'ETH'}
                },
                
            cource:{char:'', title:'', value:''}
        };
        
        this.getDataTickers = this.getDataTickers.bind(this);
        this.getTickers     = this.getTickers.bind(this);
        this.getDataQuery   = this.getDataQuery.bind(this);
    }

    componentDidMount()
    {
        this.getDataTickers();     
        this.getDataQuery('cource', '/deposit/getcource');
    }
    
    getDataTickers()
    {   
        if(this.state.tickers.length == 0){
            this.getDataQuery('tickers', '/deposit/gettickers');
        }
        
        setInterval(()=>
        {
           this.getDataQuery('tickers', '/deposit/gettickers');
        }, 5000);
    }
    
    getDataQuery(name, path)
    {
        fetch(this.props.param.url+path, 
        {
            method: 'POST',
            credentials: this.props.param.same
        }
        ).then(function (response)
        {
            return response.json();
        }
        ).then(data => {
            var obj = {};
            obj[name] = data;
            this.setState(obj);                  
        });
    }
  
    getTickers()
    {
        return this.state.tickers.map(
                
            (ticker, i)=>
            
                <div key={i} className="col-md-3">
                    <h4>
                        {
                            this.state.titles[ticker.SYMBOL]['name'] + ' (' + 
                            this.state.titles[ticker.SYMBOL]['code'] + ')'
                        }
                    </h4>
                    <span className="last_price">
                        {ticker.LAST_PRICE.toFixed(3) + ' '} 
                        <i>
                            USD 
                        </i> 
                        <span className={ticker.DAILY_CHANGE_PERC < 0 ? 'text-danger' :''} >
                                {' ' + Math.abs((ticker.DAILY_CHANGE_PERC * 100).toFixed(1)) + ' %'}
                        </span>
                    </span>
                </div>
        );
    }
    
    render() {
        return (
            <div className="col-md-12 alert alert-info ticker">
                <div className="ticker_block">
                    <h3 className="col-md-12" >Курсы</h3>
                    <div className="row">
                        {this.getTickers()}
                        <div className="col-md-3">
                            <h4>Курс рубля к доллару</h4>             
                            <span>
                                {'USD: '+this.state.cource.value + ' руб.'}
                            </span>
                        </div>
                    </div>                     
                </div>
            </div>
        );
    }
}
export default Ticker;
