import React, {Component} from 'react';
import {connect} from 'react-redux';
import params from "../../sustem/params";


class SumPayments extends Component {


    getBalances()
    {
        return this.props.balances.map((balance, key) => <p key={key} >Всего: { (+balance.sum).toFixed(params.numLength) + ' ' +balance['currency_code']}</p>);
    }

    render() {
        return (
            <div className="sum-payments-block pull-right">
                {this.getBalances()}
            </div>
        );
    }
}

export default connect(
    state => ({
       titles:state.language
    }),
    dispatch => ({
        
    })
  )(SumPayments);