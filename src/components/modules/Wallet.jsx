import React, { Component } from 'react';
import {connect} from 'react-redux';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {generateWalletAction} from "../../actions/deposit";
import params from "../../sustem/params";


class Wallet extends Component
{

    constructor(props)
    {
        super(props);

        this.state = {
          address:'',
          load:'hidden el-load',
          copied_adress: false,
          copied_label: false
        };
        
        this.generate   = this.generate.bind(this);
        this.copyAdress = this.copyAdress.bind(this);
        this.copyChange = this.copyChange.bind(this);
        this.copyLabel  = this.copyLabel.bind(this);
    }

    generate = () =>
    {    
        let data = {
            id:this.props.data.id,
            wkey:this.props.wkey,
            currency_id:this.props.data.currency_id,
            code:this.props.data.code
        }

        this.props.generateWallet(data, this.changeLoad);
    }

    changeLoad = () => {
        let loaded = '';
        (this.state.load === 'hidden el-load') ? loaded = 'show el-load' : loaded = 'hidden el-load';
        this.setState({load:loaded});
    }
    
    copyAdress(){ 
       this.copyChange('copied_adress');
    }
    
    copyLabel(){
        this.copyChange('copied_label');
    }
    
    copyChange(name)
    {
        let obj = {};
        obj[name] = true;
        this.setState(obj);
        setTimeout(()=>{
            obj[name] = false;
            this.setState(obj);
        }, 2000);
    }
    
    render() {
     
      return (
            <div className="col-sm-12 wallet_block">

                <div className="block_header col-sm-2">
                    <div className="img-block">
                        <img src={params.path_img+'wallet/'+this.props.data.name+'.png'} /> 
                        <span className="title-block">{this.props.data.title}</span>
                    </div>
                    <div className="button-generate form-group">
                        <button onClick={this.generate} className="btn btn-success" type="button">Создать</button>
                        <div className={this.state.load} >
                            <i className="fa fa-cog fa-spin fa-3x fa-fw"></i>
                        </div>
                    </div>
                </div>

                <div className="wallet col-sm-10 pull-right">    

                    <div className={this.props.data.address ? 'row wallet_adress' : 'hidden'}>
                        <label>
                            <b>Адрес:</b>
                        </label>
                        <input 
                            className={this.state.copied_adress ? 'col-sm-10 text-success' : 'col-sm-10'}
                            value={this.props.data.address ? this.props.data.address : ''} readOnly
                        />
                        <span className="col-sm-2 copy-wrap">
                            <CopyToClipboard text={this.props.data.address}
                                onCopy={this.copyAdress}>
                                <span className="copy_address">
                                    <i className="fa fa-files-o" aria-hidden="true"></i>
                                    Скопировать
                                </span>
                            </CopyToClipboard>
                        </span>
                    </div>

                    <div className={this.props.data.label ? 'row wallet_label' : 'hidden'}>                              
                        <label>
                            <b>Ключ:</b>
                        </label>

                        <input 
                            className={this.state.copied_label ? 'col-sm-10 text-success' : 'col-sm-10'}
                            value={this.props.data.label ? this.props.data.label: ''} readOnly
                        />

                        <span className="col-sm-2 copy-wrap">
                            <CopyToClipboard text={this.props.data.label}
                                onCopy={this.copyLabel}>                               
                                <span className="copy_address">
                                    <i className="fa fa-files-o" aria-hidden="true"></i>
                                    Скопировать
                                </span>
                            </CopyToClipboard>
                        </span>
                    </div>
              </div>
          </div>
      );
    }
}
export default connect(
    state => ({}),
    dispatch => ({
        generateWallet: (dataWallet, changeLoad) => {
            dispatch(generateWalletAction(dataWallet, changeLoad));
        }
    })
)(Wallet);