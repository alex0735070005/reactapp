import React, { Component } from 'react';
import { connect } from 'react-redux';

import CardInput    from './CardInput';
import CryptoInput  from './CryptoInput';
import NumberInput  from './NumberInput';
import YandexInput  from './YandexInput';
import QiwiInput    from './QiwiInput';
import AmountInput  from './AmountInput';
import Patterns     from './Patterns';
import Validator from '../../../service/Validator';
import {getBalanceAction} from "../../../actions/pay";

class RecipientRow extends Component
{

    constructor(props)
    {
        super(props);

        this.state = {
            effect_val: '', pay_method_slug:'sustem',
            showAmountErr:false
        };

        this.effect           = this.effect.bind(this);
        this.setMethod        = this.setMethod.bind(this);
    }


    getError(err){
      if(err){
        return 'error_field';
      }
      return 'success_field';
    }

    getErColor(err){
      if(err){
        return 'form-control warning';
      }
      return 'form-control';
    }

    getMethods()
    {
        return this.props.payment_methods.map((method, i) => <option key={i} value={i} >{method.name}</option>);
    }

    effect()
    {
        setTimeout(()=>{
            this.setState({effect_val:'show_block'});
        }, 700);

        this.setState({effect_val:'hide_block'});
    }

    changeMethod = (e)=>{
        this.setMethod(e.target.value);
        let method = this.props.payment_methods[e.target.value];
        this.props.changeField(e.target);
        if(method['code'] === 'payCrypto')
        {
            this.props.getBalance(method);
        }
        this.effect();
    }

    upField = (e) =>
    {
        this.setState({showAmountErr:false});

        e.target.name === 'amount' && !Validator.validSum(e.target.value) ? this.setState({showAmountErr:true}) : this.props.changeField(e.target);
    }
    
    setMethod(id)
    {
        var methods = this.props.payment_methods;

        for(var m in methods)
        {
            if(methods[m]['id'] === id){
                this.setState({pay_method_slug: methods[m]['slug']});
            }
        }
    }

    remove = (e)=> {
        this.props.removeRecipient(e.target.dataset.rkey);
    }

    getCurrencies = () => {
        return this.props.currencies ? this.props.currencies.map((currency, key) => <option key={key} value={currency.code} >{currency.code}</option>) : null;
    }

    render()
    {
        return (
            <div id={"recipient-"+this.props.rkey} className={ 'recipient_block row ' + this.props.recipient.pay_method_slug }>
                <div className="col-sm-8">
                    <div className="pay_method" >
                        <label>
                            <span>{this.props.titles.pay_method}</span>
                            <select data-rkey={this.props.rkey} className="form-control" name="pay_method" value={this.props.recipient.pay_method} onChange={this.changeMethod} >
                                {this.getMethods()}
                            </select>
                        </label>
                    </div>
                    {/* Form fields block */}  
                    <div className={ 'fields ' + this.state.effect_val }>
                       {/* If method exist sustem show email field */} 
                        <div className={this.props.recipient.pay_method_slug === 'sustem' ? 'title_email show': 'hidden'} >
                            <label>
                                <span>{this.props.titles.recipient_email}:</span>
                                <input className={this.getErColor(this.props.recipient.errors.email)} data-rkey={this.props.rkey} type="email" value={this.props.recipient.email} name="email" onInput={this.upField} />
                            </label>
                        </div>
                        {/* Pay numbers block */} 
                        <div className="pay_number" >
                            {/* If method exist crypto show crypto field */} 
                            {
                                (this.props.recipient.pay_method_code === 'payCrypto')
                                ?
                                  <CryptoInput
                                      titles = {this.props.titles}
                                      rkey={this.props.rkey}
                                      inputHandler = { this.upField}
                                      valueNum     = { this.props.recipient.number}
                                  />
                                :''
                            }
                            {/* If method exist card show card field */} 
                            {
                                (this.props.recipient.pay_method_slug === 'card')
                                ?
                                  <CardInput
                                      titles = {this.props.titles}
                                      rkey={this.props.rkey}
                                      inputHandler = { this.upField}
                                      valueNum     = { this.props.recipient.number}
                                      valueDate    = { this.props.recipient.card_date }
                                      notValidNum  = {this.props.recipient.errors.number}
                                      notValidCard = {this.props.recipient.errors.card_date}
                                      code         = {this.props.recipient.pay_method_code}
                                  />
                                :''
                              }
                              {/* If method exist pay number show number field */} 
                              {
                                (this.props.recipient.pay_method_slug === 'num')
                                ?
                                  <NumberInput
                                      titles = {this.props.titles}
                                      rkey={this.props.rkey}
                                      inputHandler = { this.upField}
                                      valueNum     = { this.props.recipient.number}
                                      notValidNum  = {this.props.recipient.errors.number}
                                  />
                                :''
                              }
                              {/* If method exist yandex show yandex field */} 
                              {
                                (this.props.recipient.pay_method_slug === 'yandex')
                                ?
                                  <YandexInput
                                      titles = {this.props.titles}
                                      rkey={this.props.rkey}
                                      inputHandler = { this.upField}
                                      valueNum     = { this.props.recipient.number}
                                      notValidNum  = {this.props.recipient.errors.number}
                                  />
                                :''
                              }
                              {/* If method exist qiwi show qiwi field */} 
                              {
                                (this.props.recipient.pay_method_slug === 'qiwi')
                                ?
                                  <QiwiInput
                                      titles = {this.props.titles}
                                      rkey={this.props.rkey}
                                      inputHandler = { this.upField}
                                      valueNum     = { this.props.recipient.number}
                                      notValidNum  = {this.props.recipient.errors.number}
                                  />
                                :''
                              }
                        </div>
                    </div>
                </div>
                <div className="col-sm-4" >
                    <Patterns 
                        addPattern={this.props.addPattern} 
                        editPattern={this.props.editPattern}
                        removePattern={this.props.removePattern}
                        clearPattern={this.props.clearPattern}
                        rkey={this.props.rkey} 
                        />
                    <div className="amount-block"> 
                        <AmountInput 
                            titles={this.props.titles} 
                            amount={this.props.recipient.amount} 
                            error={this.props.recipient.errors.amount}
                            getErColor={this.getErColor}
                            rkey={this.props.rkey}
                            upField={this.upField}
                            showAmountErr={this.state.showAmountErr}
                        />
                        <select value={this.props.recipient.currency} data-rkey={this.props.rkey} className="form-control" name="currency" onChange={this.upField} >
                                {this.getCurrencies()}
                        </select>
                      
                    </div>
                    <i className="fa fa-trash-o" data-rkey={this.props.rkey} onClick={this.remove} ></i>
                </div>
                {/* Error message block */} 
                <div className="error_block">
                    <div className={this.getError(this.props.recipient.errors.email)      + ' er_email'} >{this.props.titles.er_email}</div>
                    <div className={this.getError(this.props.recipient.errors.card_date)  + ' er_card_date'} >{this.props.titles.er_card_date}</div>
                    <div className={this.getError(this.props.recipient.errors.pay_method) + ' er_paymethod'}></div>
                    <div className={this.getError(this.props.recipient.errors.number)     + ' er_number'} >{this.props.titles.er_number}</div>
                    <div className={this.getError(this.props.recipient.errors.amount)     + ' er_amount'} >{this.props.titles.er_amount}</div>
                </div>
            </div>
        );
    }
};

export default connect(
    state => ({
        currencies:state.pay.currencies
    }),
    dispatch => ({
        changeField: (d) => {
            dispatch({type:'CHANGE_FIELD', data:d});
            dispatch({type:'CHANGE_SUM_CURRENCY'});
        },
        removeRecipient: (k) => {
            dispatch({type:'REMOVE_RECIPIENT', data:{rkey:k}});
            dispatch({type:'CHANGE_SUM_CURRENCY'});
        },
        getBalance: (m) => {
            dispatch(getBalanceAction(m));
        }
    })
  )(RecipientRow);

