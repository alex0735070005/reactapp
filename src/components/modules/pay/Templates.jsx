import React, { Component } from 'react';
class Templates extends Component 
{    
    getTemplates(){
        return this.props.templates.map
        (
            (template, i) => <option key={i} value={template.id}>{template.name}</option>
        );
    }
    
    render(){
        return (
            <div className="form-group hidden">                
                <select id="template" name="template" className="form-control">
                    <option value="0">{this.props.titles.get_template}</option>
                    {this.getTemplates()}
                </select>
            </div>
        );
    }
};
export default Templates;