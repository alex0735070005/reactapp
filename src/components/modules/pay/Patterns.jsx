import React, { Component } from 'react';
import { connect } from 'react-redux';
class Patterns extends Component {

    constructor(props)
    {
        super(props);
       
        this.state = {
            selected:{key:'', id:'', name:'Шаблоны'},
            showMenu:false,
            menuVisible:'hidden',
            menuAr:'fa-sort-down',
            menuHover:false
        };
      
        //document.querySelector('body').onclick = this.closeOnBody;
    }
     
    triggerMenu = (menuState) =>
    {
        menuState ? this.setState({menuVisible:'show', menuAr:'fa-sort-up'}) : this.setState({menuVisible:'hidden', menuAr:'fa-sort-down'});
    }
    
    changeMenuVisable = () =>
    {
        let menuState = !this.state.showMenu;
        this.setState({showMenu:menuState});
        this.triggerMenu(menuState);
    }
    
    closeOnBody = () =>
    {
        if(!this.state.menuHover){
            this.setState({showMenu:false});
            this.triggerMenu(false);
        }
    }
    
    enterMenu = () =>
    {
        this.setState({menuHover:true});
    }
    
    leaveMenu = () =>
    {
        this.setState({menuHover:false});
    }
    
    remove = (e) => {
        let rkey = e.target.dataset.rkey;
        this.props.removePattern(this.state.selected.key, rkey);
    }
    
    /**
     * Select pattern object
     */
    selectOption = (el) =>
    {
        let key     = el.target.dataset.okey;
        let option  = this.props.options[key];
        let last    = document.querySelector('#select-pattern-' + this.props.rkey + ' .menu-op-' + this.state.selected.key);
            
        if(last){
            last.classList.remove('active');
        }

        el.target.classList.add('active');
 
        this.setState({selected:{key: key, id:option.id, name:option.name}});
        this.changeMenuVisable();
        this.props.selectPattern({obj:option, rkey:this.props.rkey, pkey:key});
    }
    
    componentWillReceiveProps(nextProps)
    {
        let update = false;
        nextProps.options.forEach(option => {
            if(this.state.selected === option.id)
            {
                this.setState({selected:{...this.state.selected, name:option.name}});
                update = true;
            }
        });
        if(!update){this.setState({selected:{key:'', id:'', name:'Шаблоны'}})};
    }
    
    /**
     * Get list pattern in select
     */
    getFields = () =>
    {
        return ( this.props.options.map((option, key) => <div className={'drop-el menu-op-' + key} key={key}  onClick={this.selectOption} data-okey={key} >{option.name}</div> ));
    }

    showAddPopup = (e) =>
    {
        this.props.showAddPopupPattern(this.props.rkey);
    }

    showEditPopup = () =>
    {
        this.props.showEditPopupPattern({rkey:this.props.rkey, pkey:this.state.selected.key});
    }

    showRemovePopup = () =>
    {
        this.props.showRemovePopupPattern({rkey:this.props.rkey, pkey:this.state.selected.key});
    }

    render(){
        return (
            <div className="pattern-wrap-block">   
                <div>Шаблоны</div>
                <div id={'select-pattern-' + this.props.rkey} className="select-wrap" onMouseEnter={this.enterMenu} onMouseLeave={this.leaveMenu}>
                    <div onClick={this.changeMenuVisable}  className="select-head">
                        <div className="selected">{this.state.selected.name}</div>
                        <span className={'fa ' + this.state.menuAr + ' ar-el'} ></span>
                    </div>
                    <div className={'drop-menu ' + this.state.menuVisible}>
                        <div className="drop-menu-option">
                            {this.getFields()}
                        </div>
                        <div className="drop-menu-control">
                            <span  onClick={this.showAddPopup}      data-rkey={this.props.rkey} className="btn btn-default">{this.props.titles.create}</span>
                            <span  onClick={this.showEditPopup}     data-rkey={this.props.rkey} className="btn btn-default">{this.props.titles.edit}</span>
                            <span  onClick={this.showRemovePopup}   data-rkey={this.props.rkey} className="btn btn-default">{this.props.titles.delete}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};
export default connect(
    state => ({
       options:state.pay.patterns,
       titles:state.language
    }),
    dispatch => ({
        selectPattern: (d) => {
            dispatch({type:'SELECT_PATTERN', data:d});
        },
        removeRecipient: (k) => {
            dispatch({type:'REMOVE_RECIPIENT', data:{key:k}});
        },
        showAddPopupPattern: (k) => {
            dispatch({type:'TRIGGER_ADD_POPUP_PATTERN', data:{key:k}});
        },
        showEditPopupPattern: (d) => {
            dispatch({type:'TRIGGER_EDIT_POPUP_PATTERN', data:d});
        },
        showRemovePopupPattern: (d) => {
            dispatch({type:'TRIGGER_REMOVE_POPUP_PATTERN', data:d});
        }
    })
  )(Patterns);
