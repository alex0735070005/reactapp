import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

class YandexInput extends Component {

    constructor(props)
    {
        super(props);

        this.changeNumInput = this.changeNumInput.bind(this);
    }

    changeNumInput(e)
    {
        e.target.dataset.rkey = this.props.rkey;
        this.props.inputHandler(e);
    }


    render(){
        return (
              <label>
                  <span className="title_wallet"> {this.props.titles.number_wallet}</span>
                  <NumberFormat
                      format="#### #### #### ##"
                      name="number"
                      placeholder="0000 0000 0000 00"
                      className={
                         this.props.notValidNum ? 'form-control warning' : 'form-control'
                      }
                      onInput = {this.changeNumInput}
                      value={this.props.valueNum}
                  />
              </label>
        );
    }
};
export default YandexInput;
