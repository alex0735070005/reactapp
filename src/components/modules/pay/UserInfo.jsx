import React, { Component } from 'react';
class UserInfo extends Component {    
    render(){
        return (
            <div>
                <h3>{this.props.titles.pay_from}:</h3>
                <span className="user_name">{this.props.name}</span>
                <div className="balance_block">
	                <span className="b_name">{this.props.titles.balance}: </span>
	                <span className="b_val">{this.props.balance.value} {this.props.titles.rub}</span>
                </div>
            </div>    
        );
    }
};
export default UserInfo;