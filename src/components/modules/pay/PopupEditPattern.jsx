import React, { Component } from 'react';
import {connect} from 'react-redux';
import { editPatternAction } from '../../../actions/pattern';

class PopupEditPattern extends Component {

    edit = (e) => {
        e.preventDefault();
        this.props.editPattern(this.props.selectedPattern, e.target, this.props.pattern.pkey);
    }

    changeName = (e) => {
        this.props.changePatternName(e.target.value)
    }

    render()
    {   
        let show = this.props.pattern.showPopupEditPattern ? 'show' : 'hidden';

        if (this.props.pattern.showPopupEditPattern)
        {
            return (
                <div className={'popup_background ' + show}>
                    <div className = "popup_wrap">
                        <div className = "popup_body">
                            <form onSubmit={this.edit}>

                                { this.props.pattern.successEdit ? 
                                    <span className="p_succes">
                                        {this.props.titles.pattern_edit_success}
                                    </span>: '' 
                                }

                                { this.props.pattern.errorEdit ? 
                                    <span className="p_danger">
                                        {this.props.titles.not_valid_pattern}
                                    </span>: '' 
                                }

                                <label>
                                    <span>{this.props.titles.pattern_name}</span>
                                    <input name="name" className="form-control" type="text" defaultValue={this.props.selectedPattern ? this.props.selectedPattern.name:''} />                            
                                </label>

                                <button className="btn btn-success">{this.props.titles.update}</button>
                                <button type="button" className="btn btn-primary pull-right" onClick={this.props.closePopup}>{this.props.titles.close}</button>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
        return null;
    }
};

 export default connect(
    state => ({
        titles:state.language,
        pattern:state.pattern,
        selectedPattern:state.pay.patterns[state.pattern.pkey],
        recipients:state.pay.recipients
    }),
    dispatch => ({
        closePopup: () => {
            dispatch({type:'TRIGGER_EDIT_POPUP_PATTERN', data:{key:''}});
        },
        editPattern: (p, d, k) => {
            dispatch(editPatternAction(p, d, k));
        }
    })
  )(PopupEditPattern);