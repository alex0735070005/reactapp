import React, { Component } from 'react';
import RecipientRow from './RecipientRow';
import { connect } from 'react-redux';

class Recipients extends Component
{
    
    getRecipients()
    {
        return this.props.recipients.map(
            (recipient, i) => <RecipientRow  key={i} rkey={i} recipient = {recipient} titles = {this.props.titles} payment_methods = {this.props.payment_methods} />
        );
    }

    render()
    {
        return (
            <div id="recipients" className="form-group">
                {this.getRecipients()}
                <div className="form-group">
                      <button type="button" className="btn btn-success" onClick={this.props.addRecipient} >+ {this.props.titles.add_recipient}</button>
                </div>
            </div>
        );
    }
};

 export default connect(
    state => ({
        recipients:state.pay.recipients,
        titles:state.language,
        payment_methods:state.pay.payment_methods
    }),
    dispatch => ({
        addRecipient: () => {
            dispatch({type:'ADD_RECIPIENT'});
        }
    })
  )(Recipients);
