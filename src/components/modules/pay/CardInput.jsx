import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

function limit(val, max) {
  if (val.length === 1 && val[0] > max[0]) {
    val = '0' + val;
  }

  if (val.length === 2) {
    if (Number(val) === 0) {
      val = '01';

    //this can happen when user paste number
  } else if (val > max) {
      val = max;
    }
  }

  return val;
}

function cardExpiry(val) {
  let month = limit(val.substring(0, 2), '12');
  let date = limit(val.substring(2, 4), '31');

  return month + (date.length ? '/' + date : '');
}

class CardInput extends Component {

    constructor(props)
    {
        super(props);

        this.state = {
           inputValue:'',  method:'', cardType:'', erNumber:false
        };

        this.validCard      = this.validCard.bind(this);
        this.getTypeCard    = this.getTypeCard.bind(this);
        this.changeNumInput = this.changeNumInput.bind(this);
        this.changeCardInput= this.changeNumInput.bind(this);
    }

    changeNumInput(e)
    {
        e.target.dataset.rkey = this.props.rkey;
        this.getTypeCard(e);
        this.props.inputHandler(e, this.validCard(e));
    }

    changeCardInput(e)
    {
        e.target.dataset.rkey = this.props.rkey;
        this.props.inputHandler(e);
    }

    validCard(e)
    {
        var num = e.target.value.toString().replace(/[^\d]/g, '');
        var ar  = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9];
        if (!num) return false;
        var length = num.length;
        var bit = 1;
        var sum = 0;
        var value;
       
        while (length) {
          value = parseInt(num.charAt(--length), 10);
          sum += (bit ^= 1) ? ar[value] : value;
        }

        var res = true;  
        
        if(sum && sum % 10 === 0){
            res = false;
        }
        
        if(num === '' || num.length < 5) res = false;
        
        return res;
    }

    getTypeCard(e)
    {
        var card = e.target.value.toString().replace(/[^\d]/g, '');
        this.setState({cardType:''});
        //var result;
        var types = {
            electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            dankort: /^(5019)\d+$/,
            interpayment: /^(636)\d+$/,
            unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^5[1-5][0-9]{14}$/,
            amex: /^3[47][0-9]{13}$/,
            diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
            discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
            jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
            forbrugsforeningen: /^(600)\d+$/,
            mir: /^220[0-4][0-9][0-9]\d{10}$/
        };

        for (var type in types) {
            if (types[type].test(card)) {
                this.setState({cardType:type.toString()});
            }
        }
    }


    render(){
        return (
            <div>
                <label>
                    <span className="title_number" >  {this.props.titles.number_card}</span>
                    <span>{this.state.cardType}</span>
                    <NumberFormat
                        format="#### #### #### ####"
                        mask = '_'
                        name="number"
                        placeholder="0000 0000 0000 0000"
                        className={
                          !this.props.notValidNum ? 'form-control' : 'form-control warning'
                        }
                        onChange = {this.changeNumInput}
                        value={this.props.valueNum}
                    />
                </label>
                {
                (this.props.code === "providerAlfaBankCard") 
                ?
                <label>
                    <span className="title_card_date"> {this.props.titles.card_date}</span>
                    <NumberFormat
                        placeholder="mm/yy"
                        onChange = {this.changeCardInput}
                        name="card_date"
                        format={cardExpiry}
                        value={this.props.valueDate}
                        className={
                          !this.props.notValidCard ? 'form-control' : 'form-control warning'
                        }
                    />
                </label>
                :''
                }
            </div>
        );
    }
};
export default CardInput;
