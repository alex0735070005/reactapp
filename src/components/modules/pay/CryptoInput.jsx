import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

class CryptoInput extends Component {

    constructor(props)
    {
        super(props);

        this.changeNumInput = this.changeNumInput.bind(this);
    }

    changeNumInput(e)
    {
        e.target.dataset.rkey = this.props.rkey;
        this.props.inputHandler(e);
    }


    render(){
        return (
              <label>
                  <span className="title_number" >  {this.props.titles.number_wallet}</span>
                  <input name="number" className="form-control" type="text" defaultValue={this.props.valueNum} onInput={this.changeNumInput} />
              </label>
        );
    }
};
export default CryptoInput;
