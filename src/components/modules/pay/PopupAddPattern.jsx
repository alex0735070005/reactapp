import React, { Component } from 'react';
import {connect} from 'react-redux';
import { addPatternAction } from '../../../actions/pattern';

class PopupAddPattern extends Component {

    add = (e) => {
        e.preventDefault();
        let recipient = this.props.recipients[this.props.pattern.rkey]
        this.props.addPattern(this.props.pattern, recipient);
    }

    changeName = (e) => {
        this.props.changePatternName(e.target.value)
    }

    render()
    {   
        let show = this.props.pattern.showPopupAddPattern ? 'show' : 'hidden';

        if (this.props.pattern.showPopupAddPattern)
        {
            return (
                <div className={'popup_background ' + show}>
                    <div className = "popup_wrap">
                        <div className = "popup_body">
                            <form onSubmit={this.add}>

                                { this.props.pattern.successAdd ? 
                                    <span className="p_succes">
                                        {this.props.titles.pattern_add_success}
                                    </span>: '' 
                                }

                                { this.props.pattern.errorAdd ? 
                                    <span className="p_danger">
                                        {this.props.titles.not_valid_pattern}
                                    </span>: '' 
                                }

                                <label>
                                    <span>{this.props.titles.pattern_name}</span>
                                    <input name="name" className="form-control" type="text" value={this.props.pattern.name} onInput={this.changeName} />                            
                                </label>

                                <button className="btn btn-success">{this.props.titles.save}</button>
                                <button type="button" className="btn btn-primary pull-right" onClick={this.props.closePopup}>{this.props.titles.close}</button>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            return null;
        }
    }
};

 export default connect(
    state => ({
        titles:state.language,
        pattern:state.pattern,
        recipients:state.pay.recipients
    }),
    dispatch => ({
        closePopup: () => {
            dispatch({type:'TRIGGER_ADD_POPUP_PATTERN', data:{key:''}});
        },
        addPattern: (d, r) => {
            dispatch(addPatternAction(d, r));
        },
        changePatternName: (n) => {
            dispatch({type:'CHANGE_PATTERN_NAME', data:{name:n}});
        }
    })
  )(PopupAddPattern);