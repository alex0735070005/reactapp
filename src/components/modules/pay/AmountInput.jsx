import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

class AmountInput extends Component {
   
    render(){
        return (
            <label>
                <span>{this.props.titles.amount}</span>
                <input name="amount" onInput={this.props.upField} value={this.props.amount} className={this.props.getErColor(this.props.error)} data-rkey={this.props.rkey} type="text"   />
                {this.props.showAmountErr ? <span className="text-danger" >Не валидное значение пример: 00.00</span> : ''}                            
            </label>
        );
    }
};

export default AmountInput;
