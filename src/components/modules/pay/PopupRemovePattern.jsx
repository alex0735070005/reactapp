import React, { Component } from 'react';
import { connect } from 'react-redux';
import { removePatternAction } from '../../../actions/pattern';

class PopupRemovePattern extends Component {

    remove = (e) => {
        e.preventDefault();    
        this.props.removePattern(this.props.selectedPattern, this.props.pattern.rkey, this.props.pattern.pkey);
    }

    render()
    {
        let show = this.props.pattern.showPopupRemovePattern ? 'show' : 'hidden';

        if (this.props.pattern.showPopupRemovePattern)
        {
            return (
                <div className={'popup_background ' + show}>
                    <div className = "popup_wrap">
                        <div className = "popup_body">
                            <p> {this.props.titles.is_delete_pattern + ' ' + (this.props.selectedPattern ? this.props.selectedPattern.name:'')}?</p>
                            <button className="btn btn-success" onClick={this.remove} data-pid={this.props.pattern.id} >{this.props.titles.delete}</button>
                            <button type="button" className="btn btn-primary pull-right" onClick={this.props.closePopup}>{this.props.titles.close}</button>
                        </div>
                    </div>
                </div>
            );
        }
        return null;
    }
};

 export default connect(
    state => ({
        titles:state.language,
        pattern:state.pattern,
        selectedPattern:state.pay.patterns[state.pattern.pkey],
        recipients:state.pay.recipients
    }),
    dispatch => ({
        closePopup: () => {
            dispatch({type:'TRIGGER_REMOVE_POPUP_PATTERN', data:{key:''}});
        },
        removePattern: (p, r, k) => {
            dispatch(removePatternAction(p, r, k));
        }
    })
  )(PopupRemovePattern);