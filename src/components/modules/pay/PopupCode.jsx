import React, { Component } from 'react';

class PopupCode extends Component {

    render()
    {
        return (
            <div className={'popup_background ' + this.props.show}>
                <div className = "popup_wrap">
                    <div className = "popup_body">
                        <span className={'p_success ' + this.props.success}>{this.props.titles.success}</span>
                        <span className={'p_danger ' + this.props.danger}>{this.props.titles.not_valid_code}</span>
                        <label>
                            <span>{this.props.titles.input_code}</span>
                            <input className="form-control" onInput={this.props.changeCode} type="text" />                            
                        </label>
                        <button type="button" className="btn btn-success" onClick = {this.props.sendCode}>{this.props.titles.send}</button>
                        <button type="button" className="btn btn-primary pull-right" onClick = {this.props.close}>{this.props.titles.close}</button>
                    </div>
                </div>
            </div>
        );
    }
};

export default PopupCode;