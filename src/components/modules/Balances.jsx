import React, {Component} from 'react';
import {connect} from 'react-redux';
import params from "../../sustem/params";


class Balances extends Component {


    getBalances()
    {
        return this.props.balances.map((balance, key) => <span key={key} ><b>{balance['currency_code'] +': '}</b>{ (+balance.value).toFixed(params.numLength) + ' '}</span>);
    }

    render() {
        return (
            <div className="balances-block">
                {this.getBalances()}
            </div>
        );
    }
}

export default connect(
    state => ({
       titles:state.language,
       balances:state.pay.balances
    }),
    dispatch => ({
        
    })
  )(Balances);