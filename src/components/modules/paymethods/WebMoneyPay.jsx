import React, {Component} from 'react';
import {connect} from 'react-redux';
import { webMoneyPayAction } from '../../../actions/deposit';
import params from "../../../sustem/params";

class WebMoneyPay extends Component {

    changeSum = (e) => {
        this.props.changeSumPay({method:'webMoney', sum:e.target.value});
    }

    sendPay = (e) => {
        this.props.pay(this.props.webMoney);
    }

    render() {
        return (
            <div className="pay-method">
                <img src={params.path_img+'webmoney.png'} alt="Webmoney" />
                 <b>Оплата на: {this.props.code==='RUB'?'WMR':'WMZ'} </b> 
                <input id="qiwi-pay-input" placeholder="Введите сумму:"  value={this.props.webMoney.sum} type="text" onInput={this.changeSum} /> 
                <button type="button" onClick={this.sendPay} > Оплатить </button>
            </div>
        );
    }
}

export default connect(
    state => ({
       webMoney:state.payment.webMoney
    }),
    dispatch => ({
        pay: (dataPay) => {
            dispatch(webMoneyPayAction(dataPay));
        },

        changeSumPay: (data) => {
            dispatch({type:'CHANGE_SUM_PAY', data});
        }
    })
  )(WebMoneyPay);