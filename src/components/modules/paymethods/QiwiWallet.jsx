import React, {Component} from 'react';
import {connect} from 'react-redux';
import { qiwiPayAction } from '../../../actions/deposit';
import params from "../../../sustem/params";

class QiwiWallet extends Component {

    changeSum = (e) => {
        this.props.changeSumPay({method:'qiwiWallet', sum:e.target.value});
    }

    sendPay = (e) => {
        this.props.pay(this.props.qiwiWallet);
    }

    render() {
        return (
            <div className="pay-method">
                <img src={params.path_img+'qiwi.png'} alt="QiwiWallet" />
                 <b>Оплата с кошелька: </b> 
                <input id="qiwi-pay-input" placeholder="Введите сумму:" type="text" value={this.props.qiwiWallet.sum} onInput={this.changeSum} /> 
                <button type="button" onClick={this.sendPay} > Оплатить </button>
            </div>
        ); 
    }
}

export default connect(
    state => ({
       qiwiWallet:state.payment.qiwiWallet
    }),
    dispatch => ({
        pay: (dataPay) => {
            dispatch(qiwiPayAction(dataPay));
        },

        changeSumPay: (data) => {
            dispatch({type:'CHANGE_SUM_PAY', data});
        }
    })
)(QiwiWallet);