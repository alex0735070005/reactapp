import React, {Component} from 'react';
import {connect} from 'react-redux';
import { qiwiPayAction } from '../../../actions/deposit';
import params from "../../../sustem/params";

class QiwiPay extends Component {

    changeSum = (e) => {
        this.props.changeSumPay({method:'qiwiPay', sum:e.target.value});
    }

    sendPay = (e) => {
        this.props.pay(this.props.qiwiPay);
    }

    render() {
        return (
            <div className="pay-method">
                <img src={params.path_img+'qiwi.png'} alt="QiwiPay" />
                 <b>Оплата карточкой: </b> 
                <input id="qiwi-pay-input" placeholder="Введите сумму:" type="text" value={this.props.qiwiPay.sum} onInput={this.changeSum} /> 
                <button type="button" onClick={this.sendPay} > Оплатить </button>
            </div>
        );
    }
}

export default connect(
    state => ({
       qiwiPay:state.payment.qiwiPay
    }),
    dispatch => ({
        pay: (dataPay) => {
            dispatch(qiwiPayAction(dataPay));
        },

        changeSumPay: (data) => {
            dispatch({type:'CHANGE_SUM_PAY', data});
        }
    })
  )(QiwiPay);