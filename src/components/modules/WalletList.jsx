import React, {Component} from 'react';
import {connect} from 'react-redux';
import Wallet from './Wallet';

class WalletList extends Component {

    getWallets = () => {
        return this.props.wallets.map(
            (wallet, k) => {
                if(wallet.currency_id === this.props.data.id){
                    return <Wallet data={wallet} key={k} wkey={k} />
                }    
            }
        );
    }

    render() {
        return (
          
            <div className="currency-wallets"> 
                {this.getWallets()}
            </div>
        );
    }
}

export default connect(
    state => ({
        wallets:state.deposit.wallets        
    }),
    dispatch => ({
       
    })
  )(WalletList);