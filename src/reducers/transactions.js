import Pagger from '../service/Pagger';

let pagger =  new Pagger();

const initState = {pagger : pagger};

export default function transactions(state = initState, action)
{
    switch(action.type)
    {
        case 'UP_PAGGER':

        return {
            ...state,
            pagger: {...state.pagger, data:action.data.items, pages:action.data.total, limit: action.data.limit}
        }

        default: return state;     
    }
}
