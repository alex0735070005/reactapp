
import Recipient from '../service/Recipient';   
import Errors from '../service/Errors';


let recipient = new Recipient();

var initState = {
    name:'',
    id:'',
    balances:[],
    patterns:[],
    currencies:[],
    recipients:[recipient],
    payment_methods:[]
}

export default function pay(state = initState, action)
{
    switch(action.type)
    {
        case 'GET_PAY_DATA':    

        return {
            ...state,
            name:action.data.name,
            id:action.data.id,
            balances:action.data.balances.map((balance)=>{
                return {...balance, sum:0};
            }),
            patterns:action.data.patterns,
            currencies:action.data.currencies,
            recipients:[new Recipient()],
            payment_methods:action.data.payment_methods
        }

        case 'CHANGE_BALANCES':  
        
        return {
            ...state,
            balances:[...action.data]
        }

        case 'ADD_RECIPIENT':  

        return {
            ...state,
            recipients:[
                ...state.recipients,
                new Recipient()
            ]
        }

        case 'REMOVE_RECIPIENT':
        
        return {
            ...state,
            recipients:[
                ...state.recipients.slice(0, +action.data.rkey),
                ...state.recipients.slice(+action.data.rkey+1)
            ]
        };
        

        case 'CHANGE_ERROR_RECIPIENT': 
    
        return {
            ...state,
            recipients:state.recipients.map((recipient, key) => 
            {
                if(key === + action.data.rkey)
                {
                    let newRecipient =  {
                        ...recipient,
                        errors:{...recipient.errors},
                    }

                    newRecipient.errors[action.data.name] = action.data.value;

                    return newRecipient;
                }
                return recipient;
            })
        }

        case 'NULL_ERROR_RECIPIENT':      
            
        return {
            ...state,
            recipients:state.recipients.map((recipient, key) => 
            {
                if(key === +action.data.rkey)
                {
                    return {
                        ...recipient,
                        errors:new Errors()
                    }
                }
                return recipient;
            })
        }

        case 'CHANGE_FIELD':  

        return {
            ...state,
            recipients:state.recipients.map((recipient, key) => 
            {
                if(key === +action.data.dataset.rkey)
                {
                    let newRecipient =  {
                        ...recipient,
                        errors:{
                            ...recipient['errors'],

                        },
                    }

                    if(action.data.name === 'pay_method')
                    {
                        let method = getMethod(action.data.value, state.payment_methods);

                        newRecipient = new Recipient();

                        newRecipient['pay_method_code'] = method['code'];
                        newRecipient['pay_method_slug'] = method['slug'];
                    }

                    newRecipient[action.data.name] = action.data.value;

                    return newRecipient;
                }
                return recipient;
            })
        }

        case 'CHANGE_SUM_CURRENCY':      
            
        return {
            ...state,
            balances:state.balances.map((balance) => 
            {
                return {
                    ...balance,
                    sum:
                    (()=>
                    {
                        let sum = 0;
                        state.recipients.forEach((r)=>{
                            sum += r.currency === balance.currency_code ? +r.amount : 0;
                        })
                        return sum;
                    })()             
                }
                
            })
        }

        /*PATTERNS METHODS*/

        // Select pattern
        case 'SELECT_PATTERN':

        return {
            ...state,
            recipients:state.recipients.map((recipient, key) =>
            {
                if(+action.data.rkey === key){
                    return{
                        ...action.data.obj.value,
                        errors:{...recipient.errors}
                    }
                }
                return recipient;
            })
        } 

        // Add pattern
        case 'ADD_PATTERN':

        return {
            ...state,
            patterns:[
                {...action.data.pattern},
                ...state.patterns
            ]
        };

        // Edit pattern
        case 'EDIT_PATTERN':

        return {
            ...state,
            patterns:state.patterns.map((pattern, key) => 
            {
                if(+action.data.pkey === key){
                    return {...pattern, ...action.data.pattern}
                }
                return pattern;
            })
        };

        // Remove pattern
        case 'REMOVE_PATTERN':

        return {
            ...state,
            recipients:state.recipients.map((recipient, key) =>
            {
                if(+action.data.rkey === key)
                {
                    return new Recipient();
                }
                return recipient;
            }),
            patterns:[
                ...state.patterns.slice(0, action.data.pkey),
                ...state.patterns.slice(action.data.pkey+1)
            ]
        }
        default: return state;     
    }   
}

function getMethod(id, methods)
{    
    for(var j in methods)
    {
        if((+id) === (+j)){
            
            return methods[j];
        }
    }
}