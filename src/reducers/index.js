import {combineReducers} from 'redux';

import deposit from './deposit';
import language from './language';
import payment from './payment';
import pay from     './pay';
import transactions from './transactions';
import header from './header';
import pattern from './pattern';
import info from './info';


export default combineReducers({
    language,
    deposit,
    payment,
    pay,
    transactions,
    header,
    pattern,
    info
});