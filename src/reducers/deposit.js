import Deposit from "../service/Deposit";

const initState = new Deposit();

export default function deposit(state = initState, action)
{
    switch(action.type)
    {
        case 'GET_DEPOSIT_DATA':
            return {
                ...state,
               currencies:action.data.currencies.map(currency =>{
                    return {...currency, value:'', navname:''}
               }),
               wallets:action.data.wallets,
               balances:action.data.balances
            }

        case 'CHANGE_WALLET':
        
            const wallets = state.wallets.map((wallet, key)=>
            {
                if(+action.data.wkey !== key){
                    return wallet;
                }

                return {
                    ...wallet,
                    address:action.data.address,
                    label:action.data.label
                }
            })
          
            return {
                ...state,
                wallets: wallets
            }

        case 'GET_RUB_COURSE':
      
        return {
            ...state,
            rub_course:action.data.course
        }

        case 'GET_CRIPT_COURSE':
      
        return {
            ...state,
            cript_course:action.data.course
        }


        case 'CHANGE_CURRENCY_VALUE':
      
        return {
            ...state,
            currencies: state.currencies.map(currency=>
            {
                if(action.data.code === currency.code){
                    return {...currency, value:action.data.value}
                }
                if(currency.code === 'USD'){
                    return {...currency, value:(action.data.value/state.rub_course).toFixed(2)}
                }
                if(currency.code === 'RUB'){
                    return {...currency, value:(action.data.value*state.rub_course).toFixed(2)}
                }
            })
        }

        case 'CHANGE_NAV_NAME_CURRENCY':
      
        return {
            ...state,
            currencies: state.currencies.map(currency =>
            {
                if(action.data.code === currency.code && action.data.navname !== currency.navname){
                    return {...currency, value:'', navname:action.data.navname}
                }
                return {...currency, value:'', navname:''};
            })
        }

        default: return state;     
        
    }
}
