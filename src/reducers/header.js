
import navlist from '../sustem/navlist';


const initState = {
    navList:navlist,
    activeNav:0
};


export default function header(state = initState, action)
{
    switch(action.type)
    {
        case 'CHANGE_ACTIVE':
                   
        return {
            ...state,
            navList:state.navList.map((nav, key)=>
            {
                if(+action.data.nkey === key || action.data.nkey === nav.url)
                {
                    return {...nav, active:true }
                }
                return {...nav, active:false }
            }),
            activeNav:action.data.nkey                
        };

        
        default:return state;
    }
}