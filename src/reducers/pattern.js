
const initState = {
    showPopupAddPattern:false,
    showPopupEditPattern:false,
    showPopupRemovePattern:false,
    successAdd:false,
    errorAdd:false,
    successEdit:false,
    errorEdit:false,
    rkey:'',
    pkey:'',
    name:'',
    description:''
}
                                                                                                                                    
export default function pattern(state = initState, action)  
{
    switch(action.type)
    {
        // Show and hide pattern method
        case 'TRIGGER_ADD_POPUP_PATTERN':

        return {
                ...state,
                showPopupAddPattern: action.data.show!== undefined ? action.data.show :!state.showPopupAddPattern,
                rkey:action.data.key
            };
    

        // Show and hide pattern method
        case 'TRIGGER_EDIT_POPUP_PATTERN':

        return {
                ...state,
                showPopupEditPattern: action.data.show!== undefined ? action.data.show :!state.showPopupEditPattern,
                rkey:action.data.rkey,
                pkey:action.data.pkey,
            };

        // Show and hide pattern method
        case 'TRIGGER_REMOVE_POPUP_PATTERN':

        return {
                ...state,
                showPopupRemovePattern: action.data.show!== undefined ? action.data.show :!state.showPopupRemovePattern,
                rkey:action.data.rkey,
                pkey:action.data.pkey,
            };


        // Show and hide pattern method
        case 'TRIGGER_SUCCESS_ADD_PATTERN':

        return {
                ...state,
                successAdd: !state.successAdd,
            };

        // Show and hide pattern method
        case 'TRIGGER_ERROR_ADD_PATTERN':

        return {
                ...state,
                errorAdd: !state.errorAdd,
            };

        // Show and hide pattern method
        case 'TRIGGER_SUCCESS_EDIT_PATTERN':

        return {
                ...state,
                successEdit: !state.successEdit,
            };

        // Show and hide pattern method
        case 'TRIGGER_ERROR_EDIT_PATTERN':

        return {
                ...state,
                errorEdit: !state.errorEdit,
            };

        // change selected pattern name
        case 'CHANGE_PATTERN_NAME':

        return {
                ...state,
                name:action.data.name
            };

        default: return state;     
    }
}
