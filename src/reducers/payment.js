import paymethods from '../sustem/paymethods';


export default function payment(state = paymethods, action)
{
    switch(action.type)
    {
        case 'CHANGE_SUM_PAY':

        let newState = {...state};

        newState[action.data.method] =  {...newState[action.data.method], sum:action.data.sum};

        return newState;

        default: return state;     

    }
    
}