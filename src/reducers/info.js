
const initState = {
    title:'',
    message:'',
    show:false
}

export default function info(state = initState, action)
{
    switch(action.type)
    {
        case 'SHOW_INFO':

        return {
            ...state,
            show:true
        }

        case 'HIDE_INFO':

        return {
            ...state,
            show:false
        }

        case 'SET_DATA_INFO':
        
        return {
            ...state,
            title:action.data.title,
            message:action.data.message
        }

        default: return state;        
    }
}
