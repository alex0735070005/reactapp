import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Deposit      from '../components/pages/Deposit';
import Pay          from '../components/pages/Pay';
import Transactions from '../components/pages/Transactions';
import Header from '../components/header/Header';



class App extends Component 
{
  render(){
    return (
      <div>
          <Header />
          <Switch>
              <Route path='/deposit' component={Deposit}/>
              <Route path='/pay' component={Pay}/>
              <Route path='/transactions' component={Transactions}/>
              <Redirect from="/" to="/deposit"/>
          </Switch>
      </div>
    )
  };
}
export default App;